import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NewChatBotComponent } from './new-chat-bot/new-chat-bot.component';

const routes: Routes = [
  // { path: '', redirectTo: 'chatbot', pathMatch: 'full' },
  { path: 'home', component: NewChatBotComponent },
  // { path: 'modules', loadChildren: './modules/modules.module#ModulesModule' },
  //{path:'services',loadChildren:'./modules/modules.module#ModulesModule'  },
  { path: '**', redirectTo: '/home' , pathMatch: 'full'},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

