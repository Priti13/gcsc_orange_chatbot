import { Component } from '@angular/core';
// import "../assets/js/bundle.js"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'chatbot';
  public catsservices = false;
  public services = false;
  public hardware = false;

  catsservicesClick(data: any) {
    if (data = 'CATSService') {
      this.catsservices = !this.catsservices;
    } else {
      this.catsservices = false;
    }
  }

  servicesClick(data: any) {
    if (data = 'Services') {
      this.services = !this.services;
    } else {
      this.services = false;
    }
  }

  hardwareClick(data: any) {
    if (data = 'Hardware') {
      this.hardware = !this.hardware;
    } else {
      this.hardware = false;
    }
  }
}
